import Vue from 'vue'
import Router from 'vue-router';
import App from './App.vue'
import Routes from './routes'
import Resource from 'vue-resource'
import * as VueGoogleMaps from "vue2-google-maps";


Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyD1cCE2U1eVYCYHiMtO_60ZaITj9pTejpU",
    libraries: "places",
    region: "ru,en"
  }
});

Vue.use(Resource);
Vue.use(Router);


Vue.router = new Router({
	routes: Routes,
	mode: 'history'
});

Vue.http.options.root = 'https://api-demo.websanova.com/api/v1';

Vue.use(require('@websanova/vue-auth'), {
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http: require('@websanova/vue-auth/drivers/http/vue-resource.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js')
});


new Vue({
  el: '#app',
  render: h => h(App),
  router: Vue.router
});
