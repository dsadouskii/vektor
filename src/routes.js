import Landing from './components/landing/landing.vue'
// import Requisites from './components/requisites.vue'
import Admin from './components/admin/admin.vue'
import Login from './components/admin/login.vue'
import SingleNews from './components/news/singleNews.vue'
import CreateNews from './components/news/createNews.vue'
import CreateProject from './components/projects/createProject.vue'
import EditNews from './components/news/editNews.vue'
import EditProject from './components/projects/editproject.vue'

export default[
	{path: '/', component: Landing, meta: {auth: false}},
	// {path: '/requisites', component: Requisites},
	{path: '/admin', component: Admin, meta: {auth: true}},
	{path: '/news/:id', component: SingleNews, meta: {auth: false}},
	{path: '/login', component: Login, meta: {auth: false}},
	{path: '/admin/createnews', component: CreateNews, meta: {auth: true}},
	{path: '/admin/createproject', component: CreateProject, meta: {auth: true}},
	{path: '/admin/editnews/:id', component: EditNews, meta: {auth: true}},
	{path: '/admin/editproject/:id', component: EditProject, meta: {auth: true}}
]
